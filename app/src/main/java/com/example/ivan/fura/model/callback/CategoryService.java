package com.example.ivan.fura.model.callback;

import com.example.ivan.fura.model.Category;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by IVAN on 19.10.2017.
 */

public interface CategoryService {
    @GET("/api/test/categories/")
    Call<List<Category>> getCategory();
}
