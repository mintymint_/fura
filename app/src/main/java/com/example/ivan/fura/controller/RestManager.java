package com.example.ivan.fura.controller;

import com.example.ivan.fura.model.callback.CategoryService;
import com.example.ivan.fura.model.callback.PlacesService;
import com.example.ivan.fura.model.helper.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by IVAN on 18.10.2017.
 */

public class RestManager {

    private PlacesService mPlacesService;
    private CategoryService mCategoryService;

    public PlacesService getPlacesService(){
        if(mPlacesService == null){

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.HTTP.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mPlacesService = retrofit.create(PlacesService.class);

        }
        return mPlacesService;
    }


    public  CategoryService getCategoryService(){
        if(mCategoryService == null){

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.HTTP.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mCategoryService = retrofit.create(CategoryService.class);
        }

        return  mCategoryService;
    }
}
