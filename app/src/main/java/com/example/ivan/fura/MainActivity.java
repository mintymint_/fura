package com.example.ivan.fura;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.ivan.fura.controller.RestManager;
import com.example.ivan.fura.model.Example;
import com.example.ivan.fura.model.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private RestManager mRestManager;
    //Places(Info for Markers)
    Call<Example> callExample;
    ArrayList<Places> arrayPlaces;
    ////////Category(Image for Markers)
    //ArrayList<Category> arrayCategoryImage;
   // Call<List<Category>> callCategory;

    //TextView mTextView;
    private GoogleMap mMap;
    //ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_main);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

       // mTextView = (TextView) findViewById(R.id.textV);
       // mImageView = (ImageView) findViewById(R.id.Marker);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        mRestManager = new RestManager();
        callExample = mRestManager.getPlacesService().getDataExample();
        //callCategory = mRestManager.getCategoryService().getCategory();
        try {
            arrayPlaces = new ArrayList<>();
           // arrayCategoryImage = new ArrayList<>();
           // Response<List<Category>> responseCategory = callCategory.execute();
            Response<Example> responsePlaces = callExample.execute();
            Example a = responsePlaces.body();
           // for (Category c : responseCategory.body()) {
           //     arrayCategoryImage.add(c);
           //     mImageView.setImageURI(Uri.parse(arrayCategoryImage.get(1).getMarkerUrl().toString()));
            //}
            arrayPlaces.addAll(a.getData());
            //mTextView.setText(arrayPlaces.get(1).getName());

            Log.i("Output", "Result GET");
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("Er", "Error");
        }
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng index;

        // Add a marker in Sydney and move the camera
       // arrayPlaces.get(1).getLat();
        for(int i =0; i<arrayPlaces.size();i++) {
            index = new LatLng(arrayPlaces.get(i).getLat(), arrayPlaces.get(i).getLng());
            if(arrayPlaces.get(i).getCategoryId() == 1){
                mMap.addMarker(new MarkerOptions().position(index).title(arrayPlaces.get(i).getName()).icon(BitmapDescriptorFactory.fromResource(R.drawable.parking_1)));

            }
            else {
                mMap.addMarker(new MarkerOptions().position(index).title(arrayPlaces.get(i).getName()).icon(BitmapDescriptorFactory.fromResource(R.drawable.gastation_2)));
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLng(index));
        }

    }



}
