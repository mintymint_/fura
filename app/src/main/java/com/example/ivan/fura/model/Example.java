package com.example.ivan.fura.model;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Example {

    @SerializedName("pageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("data")
    @Expose
    private ArrayList<Places> data = null;
    @SerializedName("hasMorePages")
    @Expose
    private Boolean hasMorePages;

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public ArrayList<Places> getData() {
        return data;
    }

    public void setData(ArrayList<Places> data) {
        this.data = data;
    }

    public Boolean getHasMorePages() {
        return hasMorePages;
    }

    public void setHasMorePages(Boolean hasMorePages) {
        this.hasMorePages = hasMorePages;
    }

}
