package com.example.ivan.fura.model.callback;

import com.example.ivan.fura.model.Example;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by IVAN on 18.10.2017.
 */

public interface PlacesService {

    @GET("/api/test/places/")
    Call<Example> getDataExample();
}
